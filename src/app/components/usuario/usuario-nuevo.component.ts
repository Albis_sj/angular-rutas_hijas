import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-nuevo',
  templateUrl: './usuario-nuevo.component.html',
  styleUrls: ['./usuario-nuevo.component.css']
})
export class UsuarioNuevoComponent implements OnInit {

  constructor(private router : ActivatedRoute) { 
    this.router.params.subscribe(parametros => {
      console.log('ruta hija - usuario nuevo');
      console.log(parametros);
    });
    this.router.parent?.params.subscribe(parametros => {
      console.log('ruta padre - usuario nuevo');
      console.log(parametros);
      
      
    })

  }

  ngOnInit(): void {
  }

}
